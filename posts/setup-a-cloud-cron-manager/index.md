<!--
.. title: Setup a cloud cron manager
.. slug: setup-a-cloud-cron-manager
.. date: 2022-11-19 13:36:56 UTC-05:00
.. tags: infrastructure docker fly.io
.. category: Tutorials
.. link: 
.. description: A simple setup for a cron on a cloud with up to second precision
.. type: text
.. status: 
-->


A cron scheduler is a invaluable tool, usually local tools like linux crontab are 
good solutions, but. ¿What if we want to deploy a cron for free, on a public cloud, 
with capacity to execute HTTP requests, gRPC remote procedure calls, local scripts and more? Just keep reading.
<!-- TEASER_END -->

# Cron

What is and which value can you get from?

## Dkron

A nice and open source alternative

### REST API

Dkron rest api calls

### Executors

#### HTTP request

#### RPC request

#### Local scripts

## Cronitor

### Monitoring Dkron jobs 

## A Docker image

### Basic Auth

## Deploying on Fly.io
